<?php

namespace MyPackage;

class MyPackage
{
    public function hello(): string
    {
        return 'Hello world, from my package!';
    }
}